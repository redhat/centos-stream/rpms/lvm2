#!/bin/bash

set -xv

DIR="$1"
LIST="$2"

[[ -n "$DIR" ]] || {
  echo "ERROR: Missing DIR" >&2
  exit 1
}

cd "$DIR"

# TODO: use find and xargs (?)

passed_logs() {
  grep '\(passed\|skipped\|warnings\)$' list | sed 's|/|_|' | sed 's| .*|.txt|'
}

# Remove log file if the test succeeded (we are generating tons of logs)
echo "Removing output of passed tests..."
#passed_logs

for f in $(passed_logs); do
  #echo "  $f"
  rm -f "$f"
done

# rm -f $(passed_logs)

if [[ -n "$LIST" ]]; then
    cat list >> "$LIST"
fi

# Remove empty files and compress the rest:
for f in *.txt; do
  if [[ -s "$f" ]]; then
    echo "Compressing file '$f'..."
    ls -l "$f"
    gzip "$f"
    ls -l "$f.gz"
  else
    echo "Removing empty file '$f'"
    ls -l "$f"
    rm -f "$f"
  fi
done

for f in *.gz; do
if [[ -f "$f" ]]; then

  # Use tar so we do not transfer dozens of small files:
  tar cvf "logfiles.tar" *.gz

  # Clean up:
  rm -f *.gz

exit 1

fi
done

