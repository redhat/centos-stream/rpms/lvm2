#!/bin/bash

refresh_dirs() {
  local dir=
  while [[ -n "$*" ]]; do
    if [[ -d "$1" ]]; then
      rm -rf "$1"
    fi
    mkdir -p "$(dirname "$1")"
    mkdir "$1"
    shift
  done
}

refresh_dirs ~/rpmbuild/SRPMS ~/rpmbuild/RPMS
refresh_dirs /tmp/source
