From c5552b43d33ca090439b6dce90dd78cdc6286ac4 Mon Sep 17 00:00:00 2001
From: Zdenek Kabelac <zkabelac@redhat.com>
Date: Mon, 9 Dec 2024 14:33:39 +0100
Subject: [PATCH 12/21] configure.ac: add support for libnvme

Add 2 new options for linking  libnvme with lvm2.
Option  --without-libnvme, --disable-nvme-wwid

(cherry picked from commit cb87e184bcbade1ac2da8fb611177f520169decd)
---
 configure.ac           | 34 ++++++++++++++++++++++++++++++++++
 include/configure.h.in |  3 +++
 lib/Makefile.in        |  2 +-
 make.tmpl.in           |  5 ++++-
 4 files changed, 42 insertions(+), 2 deletions(-)

diff --git a/configure.ac b/configure.ac
index cbea6adc6..5d4999b2d 100644
--- a/configure.ac
+++ b/configure.ac
@@ -217,6 +217,8 @@ test "$exec_prefix" = "NONE" && exec_prefix='${prefix}'
 
 AC_ARG_WITH(blkid, [AS_HELP_STRING([--without-blkid], [do not build with blkid library])],
 	    [], with_blkid="yes")
+AC_ARG_WITH(libnvme, [AS_HELP_STRING([--without-libnvme], [do not build with libnvme library])],
+	    [], with_libnvme="yes")
 AC_ARG_WITH(systemd, [AS_HELP_STRING([--without-systemd], [do not build with systemd library])],
 	    [], with_systemd="yes")
 AC_ARG_WITH(udev, [AS_HELP_STRING([--without-udev], [do not build with udev library])],
@@ -1139,6 +1141,38 @@ AC_MSG_RESULT([$BLKID_WIPING])
 AC_DEFINE_UNQUOTED(DEFAULT_USE_BLKID_WIPING, [$DEFAULT_USE_BLKID_WIPING],
 		   [Use blkid wiping by default.])
 
+################################################################################
+dnl -- Enable nvme alternate WWID via libnvme
+AC_ARG_ENABLE(nvme-wwid,
+	      AS_HELP_STRING([--disable-nvme-wwid],
+			     [do not use libnvme to detect alternate WWIDs]),
+	      NVME_WWID=$enableval,
+	      [AS_IF([test "$with_libnvme" = "yes"], [NVME_WWID="maybe"], [NVME_WWID="no"])])
+
+# ATM NVME_WWID is the only user of libnvme, so skip checking for libnvme when disabled
+AS_IF([test "$NVME_WWID" = "no"], [with_libnvme="no"])
+
+AS_IF([test "$with_libnvme" = "yes"], [
+      PKG_CHECK_MODULES([LIBNVME], [libnvme >= 1.4], [
+		AC_CACHE_CHECK([for NVME_NIDT_CSI in libnvme.h],
+			[ac_cv_have_libnvme_csi],
+			[AC_COMPILE_IFELSE([AC_LANG_PROGRAM([#include <libnvme.h>
+const int a = NVME_NIDT_CSI;
+			])], [ac_cv_have_libnvme_csi="yes"], [ac_cv_have_libnvme_csi="no"])])
+
+		AS_IF([test "$NVME_WWID" != "no"], [
+		      AC_IF_YES(ac_cv_have_libnvme_csi, [NVME_WWID="yes"
+				AC_DEFINE(NVME_SUPPORT, 1, [Use libnvme for WWID.])],
+				[NVME_WWID="error"])])
+      ], [AS_IF([test "$NVME_WWID" = "yes"], [NVME_WWID="error"])])
+], [AS_IF([test "$NVME_WWID" = "yes"], [NVME_WWID="error"])])
+
+AS_IF([test "$NVME_WWID" = "error"],
+	[AC_MSG_ERROR([--enable-nvme-wwid requires libnvme library >= 1.1. (--with-libnvme=$with_libnvme)])])
+
+AC_MSG_CHECKING([whether to use libnvme for alternate WWIDs])
+AC_MSG_RESULT([$NVME_WWID])
+
 ################################################################################
 dnl -- Enable udev synchronization
 AC_MSG_CHECKING([whether to enable synchronization with udev processing])
diff --git a/include/configure.h.in b/include/configure.h.in
index 1dabd23c6..5a09f1d11 100644
--- a/include/configure.h.in
+++ b/include/configure.h.in
@@ -633,6 +633,9 @@
 /* Define to 1 to include code that uses dbus notification. */
 #undef NOTIFYDBUS_SUPPORT
 
+/* Use libnvme for WWID. */
+#undef NVME_SUPPORT
+
 /* Define to 1 to enable O_DIRECT support. */
 #undef O_DIRECT_SUPPORT
 
diff --git a/lib/Makefile.in b/lib/Makefile.in
index 8eab625aa..8424ac952 100644
--- a/lib/Makefile.in
+++ b/lib/Makefile.in
@@ -143,7 +143,7 @@ LIB_STATIC = $(LIB_NAME).a
 CFLOW_LIST = $(SOURCES)
 CFLOW_LIST_TARGET = $(LIB_NAME).cflow
 
-PROGS_CFLAGS = $(BLKID_CFLAGS) $(UDEV_CFLAGS)
+PROGS_CFLAGS = $(BLKID_CFLAGS) $(LIBNVME_CFLAGS) $(UDEV_CFLAGS)
 
 include $(top_builddir)/make.tmpl
 
diff --git a/make.tmpl.in b/make.tmpl.in
index c8a870a8e..878288938 100644
--- a/make.tmpl.in
+++ b/make.tmpl.in
@@ -57,7 +57,8 @@ PYTHON3 = @PYTHON3@
 PYCOMPILE = $(top_srcdir)/autoconf/py-compile
 
 LIBS += @LIBS@ $(SELINUX_LIBS) $(UDEV_LIBS) $(RT_LIBS) $(M_LIBS)
-LVMLIBS = $(DMEVENT_LIBS) $(READLINE_LIBS) $(EDITLINE_LIBS) $(LIBSYSTEMD_LIBS) $(BLKID_LIBS) $(AIO_LIBS) $(LIBS)
+LVMLIBS = $(DMEVENT_LIBS) $(READLINE_LIBS) $(EDITLINE_LIBS) $(LIBSYSTEMD_LIBS)\
+ $(BLKID_LIBS) $(LIBNVME_LIBS) $(AIO_LIBS) $(LIBS)
 # Extra libraries always linked with static binaries
 STATIC_LIBS = $(PTHREAD_LIBS) $(SELINUX_STATIC_LIBS) $(UDEV_STATIC_LIBS) $(BLKID_STATIC_LIBS) $(M_LIBS)
 DEFS += @DEFS@
@@ -88,6 +89,8 @@ LIBDLM_CFLAGS = @LIBDLM_CFLAGS@
 LIBDLM_LIBS = @LIBDLM_LIBS@
 LIBDLMCONTROL_CFLAGS = @LIBDLMCONTROL_CFLAGS@
 LIBDLMCONTROL_LIBS = @LIBDLMCONTROL_LIBS@
+LIBNVME_CFLAGS = @LIBNVME_CFLAGS@
+LIBNVME_LIBS = @LIBNVME_LIBS@
 LIBSANLOCKCLIENT_CFLAGS = @LIBSANLOCKCLIENT_CFLAGS@
 LIBSANLOCKCLIENT_LIBS = @LIBSANLOCKCLIENT_LIBS@
 LIBSEAGATEILM_CFLAGS = @LIBSEAGATEILM_CFLAGS@
-- 
2.47.1

