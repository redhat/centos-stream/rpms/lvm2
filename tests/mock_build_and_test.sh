#!/bin/bash

set -e
set -xv

# TODO: Use the role!
dnf install -y mock

# lsblk and blkid:
dnf install -y util-linux

# rpmbuild and rpmdev-setuptree
dnf install -y rpmdevtools rpm-build

# mock:
dnf install -y mock

# gather info about the system and environment:
set

pwd
ls -al

uname -a

rpm -qa | grep -F lvm2 | :
rpm -qa | grep -F device-mapper | :

which lsblk | :
lsblk | :
which blkid | :
blkid | :

find / -name lvm2.spec

# NOTE: The following does not work - it will download src.rpm from default repo, not the one under test...
#################################################################################
## compile the test suite - using rpmbuild and mock
#################################################################################
#rpmdev-setuptree
#
#dnf download --source lvm2
#rpm -ivh lvm2*.src.rpm
#
#find / -name '*.patch'
#exit 0
##rpmbuild -bs

################################################################################
# compile the test suite - using mock
################################################################################
if [[ -f lvm2.spec ]]; then

mock_profile() {
(
  ID=
  VERSION_ID=
  source /etc/os-release ""
  [[ -n "$ID" && -n "$VERSION_ID" ]] || exit 1
  echo "${ID}-${VERSION_ID}-$(arch)"
)
}

MOCK_PROFILE="$(mock_profile)"

rm -rf "/var/lib/mock/$MOCK_PROFILE/result/"

#mock -r "$MOCK_PROFILE" --init
#mock -r "$MOCK_PROFILE" --installdeps lvm2.spec
# FIXME: /var/str is controlled by tenv_workdir ansible variable - need an env.variable!
#mock -r "$MOCK_PROFILE" --buildsrpm --no-clean --spec lvm2.spec --sources "/var/str/source" --short-circuit=build
#ls -l

find / -name 'lvm2*.src.rpm'
mock -r "$MOCK_PROFILE" --rebuild --no-clean ~/rpmbuild/SRPMS/lvm2*.src.rpm

find / -name 'lvm2-2*.rpm'

[[ -n $(find / -name 'lvm2-testsuite-2*.rpm') ]]

ls -l
#dnf install -y ~/rpmbuild/RPMS/lvm2-testsuite-*.rpm
dnf install -y /var/lib/mock/"$MOCK_PROFILE"/result/lvm2-testsuite-2*.rpm
fi

