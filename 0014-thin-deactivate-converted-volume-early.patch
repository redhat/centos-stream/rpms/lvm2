From 878633052b22219fc1224184091531dba5b19252 Mon Sep 17 00:00:00 2001
From: Zdenek Kabelac <zkabelac@redhat.com>
Date: Thu, 19 Dec 2024 14:47:16 +0100
Subject: [PATCH 14/21] thin: deactivate converted volume early

Deactivate converted volume to pool early, so the conversion
exits early and does not leave some already created metadata
volumes that needed manual cleanup by user after command
aborted its conversion operation when the converted volume
was actually in-use  (i.e. when user tried to convert
a mounted LV into a thin-pool, 2 extra volumes needed removal).

(cherry picked from commit 6326d0093730fe945eeb4748738ddda55bf8a3c9)
---
 tools/lvconvert.c | 14 +++++++-------
 1 file changed, 7 insertions(+), 7 deletions(-)

diff --git a/tools/lvconvert.c b/tools/lvconvert.c
index 8dd8eea1b..d15e01fd7 100644
--- a/tools/lvconvert.c
+++ b/tools/lvconvert.c
@@ -3154,6 +3154,13 @@ static int _lvconvert_to_pool(struct cmd_context *cmd,
 
 	activate_pool = to_thinpool && is_active;
 
+	/* Before the conversion starts, make sure the volume is unused and can be deactivated
+	 * (as it needs to change target type) */
+	if (is_active && !to_thin && !deactivate_lv(cmd, lv)) {
+		log_error("Cannot convert logical volume %s.", display_lvname(lv));
+		return 0;
+	}
+
 	/* Wipe metadata_lv by default, but allow skipping this for cache pools. */
 	zero_metadata = (to_cachepool) ? arg_int_value(cmd, zero_ARG, 1) : 1;
 
@@ -3405,13 +3412,6 @@ static int _lvconvert_to_pool(struct cmd_context *cmd,
 		if (!(pool_lv = _lvconvert_insert_thin_layer(lv)))
 			goto_bad;
 	} else {
-		/* Deactivate the data LV  (changing target type) */
-		if (!deactivate_lv(cmd, lv)) {
-			log_error("Aborting. Failed to deactivate logical volume %s.",
-				  display_lvname(lv));
-			goto bad;
-		}
-
 		if (data_vdo) {
 			if (lv_is_vdo(lv)) {
 				if ((seg = first_seg(lv)))
-- 
2.47.1

