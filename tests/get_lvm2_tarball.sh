#!/usr/bin/sh

set -e

PKG=$1

[[ "$PKG" == lvm2 ]]

CHECK=false

cat sources | \
  while read SHA512_HEADER TARBALL EQUAL HASH; do
    if [[ "$SHA512_HEADER" == SHA512 ]]; then
      TARBALL=$(echo $TARBALL |sed 's/.\(.*\)./\1/')
      CHECK="sha512sum"
    else
      HASH="$SHA512_HEADER"
      CHECK="md5sum"
    fi
    if [[ -f "$TARBALL" ]] && "$CHECK" -c sources; then
      echo "File already downloaded and verified"
    else
      URL="https://sourceware.org/pub/$PKG/releases/$TARBALL"
      echo "Fetching '$URL'..."
      curl -k -o "$TARBALL" "$URL"
      echo "Checking hash of '$TARBALL'..."
      "$CHECK" -c sources
    fi
  done

